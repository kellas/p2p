/*
 * Use web3 v1.0 (beta)
 * docs: https://web3js.readthedocs.io/en/1.0/web3-eth-contract.html
 */


// Example privkey 0xac430fa3c2e67b9a13317c2d4a5ddce9fffb199c2b02afa659b7e0c9363ef329

const _config = {
	provider: 'https://ropsten.infura.io/alexp2ptoken',

	store_contract: {
		address : '0x9df12edf9c661ff117bf1ead887b82a4ed7c6c50',
		abi     : [{"constant":true,"inputs":[{"name":"key","type":"string"}],"name":"get","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"key","type":"string"},{"name":"value","type":"string"}],"name":"set","outputs":[],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"}]
	}

}

const web3 = new Web3( new Web3.providers.HttpProvider( _config.provider ) )

// Restore account from private key
const privkey = localStorage.privkey || prompt('Paste private key')
				localStorage.privkey = privkey

const Account = web3.eth.accounts.privateKeyToAccount( privkey )
web3.eth.defaultAccount = Account.address
web3.eth.accounts.wallet.add(Account)

// Create contract instanse from abi
const StoreContract = new web3.eth.Contract( 
					_config.store_contract.abi, 
					_config.store_contract.address 
				)
	  StoreContract.options.from = Account.address


// Store object
const Store = {
	set:function(k,v, callback){
		StoreContract.methods.set(k,v).send({
			from: Account.address,
			gas:  200000,
		}).then(function(receipt){
			if(callback) callback(receipt)
		});
	},

	get:function(k, callback){
		StoreContract.methods.get(k).call().then(function(res){
			if(callback) callback(res)
		});
	}
}


Store.set('key1', 'val1', function(receipt){
	console.log(receipt)

	Store.get('key1', function(val){
		console.log('value:', val)
	})
})