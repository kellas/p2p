
// find wallet in localStorage
function findLocalWallet(){
	window.Wallet = false

	if (!localStorage.LW_keystore) {
		return false
	}

	var w_json = {}
	try {
		w_json        = JSON.parse(localStorage.LW_keystore)
		Wallet = lightwallet.keystore.deserialize( w_json )
		
		return Wallet
	} catch(e) {
		return false
	}
}

// Create/restore wallet and save in localStorage
function createWallet(seedPhrase, password, callback){
	lightwallet.keystore.createVault({
		// 12 words & pass for keystore encrypt 
		seedPhrase : seedPhrase,
		password   : password,
	}, function (err, ks) {
		window.Wallet = ks

		ks.keyFromPassword(password, function (err, pwDerivedKey) {
			if (err) throw err;

			// generate five new address/private key pairs
			// the corresponding private keys are also encrypted
			ks.generateNewAddress(pwDerivedKey, 1);
			var addr = ks.getAddresses();

			localStorage.LW_keystore = JSON.stringify( ks.serialize() )
			
			if (callback) { callback(addr) };
		});
	});
}


function ksFromPass(password, callback){
	Wallet.keyFromPassword(password, function(err, pwDerivedKey) {
		if (err) {
			alert(err)
			callback(false)
			return
		}
		
		// just for check password
		Wallet.generateNewAddress(pwDerivedKey, 1)
		
		callback(true)
	})
}

function generateNewSeedPhrase(){
	return lightwallet.keystore.generateRandomSeed()
}


function exportPrivateKey(callback){
	var password = prompt('Please enter password', '');
	try {
		Wallet.keyFromPassword(password, function(err, pwDerivedKey) {
			var p = Wallet.exportPrivateKey(Wallet.getAddresses()[0], pwDerivedKey)
			callback(p)
		})
	} catch(e) {
		callback(false)
	}
}